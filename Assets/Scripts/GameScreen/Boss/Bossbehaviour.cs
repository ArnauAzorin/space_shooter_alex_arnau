﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Bossbehaviour : MonoBehaviour
{
    // Start is called before the first frame update
   public float speed;

   public float maxhealth = 200;

   private float currenthealth;

   public Image Healthbar;

   public GameObject Shieldboss;

   public GameObject LaserBoss;

   private bool Attackmode = false;

   public Vector2 limits;
    public GameObject explosion;
    [SerializeField] AudioSource audioSource;
    public GameObject graphics;



    void Start()
    {
        currenthealth = maxhealth; 

        Healthbar.fillAmount = currenthealth / maxhealth;

        explosion.gameObject.SetActive(false);

    }

    void Update(){

        /*transform.Translate(-speed*Time.deltaTime,0,0);

        if (transform.position.x > limits.x)
        {
            transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
            
        }else if (transform.position.x == limits.x)
        {
            transform.position = new Vector3(10, transform.position.y, transform.position.z);
            
        } */
        
        LaserBoss.SetActive(true);
        

    }
    // Update is called once per frame
    public void OnTriggerEnter2D(Collider2D other)
    {

        if(other.tag == "Bullet")
        {
            currenthealth -= 5;

            Healthbar.fillAmount = currenthealth / maxhealth; 

            if(currenthealth <= 0 ){
               StartCoroutine(DestroyBoss());
            }
        }

    }

    IEnumerator DestroyBoss(){

        

        explosion.gameObject.SetActive(true);
        audioSource.Play();
        graphics.gameObject.SetActive(false);
        
        yield return new WaitForSeconds(1.0f);
        Destroy(this.gameObject);

        SceneManager.LoadScene("YouWinScene");


    }


}
