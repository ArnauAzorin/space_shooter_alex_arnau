﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shieldboss : MonoBehaviour
{
    // Start is called before the first frame update
    public float maxhealth = 100;

   private float currenthealth;

    public Image Shieldbar;

      void Start()
    {
        currenthealth = maxhealth; 

        Shieldbar.fillAmount = currenthealth / maxhealth;
    }

    // Update is called once per frame
    public void OnTriggerEnter2D(Collider2D other)
    {

        if(other.tag == "Bullet")
        {
            currenthealth -= 5;

            Shieldbar.fillAmount = currenthealth / maxhealth; 

            if(currenthealth <= 0 ){
               Destroy(this.gameObject);
            }
        }

    }

    


}

