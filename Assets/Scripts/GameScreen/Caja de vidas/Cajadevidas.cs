﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cajadevidas : MonoBehaviour
{
    public int Speed;
    [SerializeField] AudioSource audioSource;
    public GameObject graphics;
    [SerializeField] Collider2D collider;
    // Start is called before the first frame update

    void Update()
    {
        transform.Translate(-Speed * Time.deltaTime, 0, 0);
    }
    // Update is called once per frame
    public void OnTriggerEnter2D(Collider2D other){
        
         if(other.tag == "Finish") {
            Destroy(this.gameObject);
        }
        else if (other.tag == "Player")
        {
            StartCoroutine(player());


        }
    }
    IEnumerator player()
    {

        
        audioSource.Play();
        graphics.gameObject.SetActive(false);
        collider.enabled = false;
        yield return new WaitForSeconds(1.0f);
        Destroy(this.gameObject);
    }
}
