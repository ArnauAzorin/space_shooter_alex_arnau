﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instanciarcaja : MonoBehaviour
{
    /*   public GameObject vidasprefab;

       public GameObject Vida;

       public GameObject Vida1;

       public float TimeLaunch;

       private float currentTime = 0;


       // Update is called once per frame

       void Update()
       {
           currentTime += Time.deltaTime;  

           if (Vida.gameObject == false){
               currentTime = -2;
               Instantiate(vidasprefab,new Vector3(Random.Range(-4f,4f),0,0), Quaternion.identity, this.transform);
           }else if(Vida1.gameObject == false){
                currentTime = -2;
               Instantiate(vidasprefab,new Vector3(Random.Range(-4f,4f),0,0), Quaternion.identity, this.transform);
           }
       }
   */

    public GameObject caja;

    public float TimeLaunch;

    private float currentTime = 0;


    // Update is called once per frame

    void Update()
    {
        currentTime += Time.deltaTime;

        if (currentTime > TimeLaunch)
        {
            currentTime = -2;
            Instantiate(caja, new Vector3(9f, Random.Range(-4.5f, 4.5f), 0), Quaternion.identity, this.transform);
        }
    }


}
