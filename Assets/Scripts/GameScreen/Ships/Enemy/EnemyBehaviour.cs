﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class EnemyBehaviour : MonoBehaviour
{
    public int Speed;
    public GameObject explosion;
    [SerializeField] AudioSource audioSource;
    public GameObject prop;
    public GameObject graphics;
    [SerializeField] Collider2D collider;

    private void Start()
    {
        prop.gameObject.SetActive(true);
        explosion.gameObject.SetActive(false);
    }
    void Update(){

        

        transform.Translate(-Speed*Time.deltaTime,0,0);
    }
    public void OnTriggerEnter2D(Collider2D other){
        
         if(other.tag == "Finish") {
            Destroy(this.gameObject);
            
        }
        else if(other.tag =="Bullet"){
            StartCoroutine(DestroyShip());
            
                      
        }

    }
    IEnumerator DestroyShip()
    {
        
        prop.gameObject.SetActive(false);
        explosion.gameObject.SetActive(true);
        audioSource.Play();
        graphics.gameObject.SetActive(false);
        collider.enabled = false;
        yield return new WaitForSeconds(1.0f);
        Destroy(this.gameObject);
    }

    }