﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escudo : MonoBehaviour
{
    // Start is called before the first frame update
    public int Shieldlives = 3;

    // Update is called once per frame
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
           Shieldlives --;

           if(Shieldlives == 0){
               Destroy(this.gameObject);
           }
        }
    }
    

}
