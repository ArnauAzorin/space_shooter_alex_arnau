﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciarEnemy : MonoBehaviour
{
    public GameObject enemyPrefab;

    public float TimeLaunch;

    private float currentTime = 0;
    

    // Update is called once per frame
   
    void Update()
    {
        currentTime += Time.deltaTime;
        
        if (currentTime>TimeLaunch){
            currentTime = -2;
            Instantiate(enemyPrefab,new Vector3(9f,Random.Range(-4.5f,4.5f),0), Quaternion.identity, this.transform);
        }
    }
        
    
}
