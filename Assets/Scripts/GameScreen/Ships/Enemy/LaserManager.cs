﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Laserprefab;

    public float Cadencia;

    private float currentTime = 0;

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;

        if (currentTime>Cadencia){
        Instantiate(Laserprefab,transform.position,Quaternion.identity,null);
        currentTime = 0;
        } 
        
    }
}
