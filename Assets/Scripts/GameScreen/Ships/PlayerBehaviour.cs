﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Threading;

public class PlayerBehaviour : MonoBehaviour
{
    
    public float speed;
    private Vector2 axis;
    public Vector2 limits;
    private float shootTime=0;
    public Weapon weapon;
    public GameObject prop1;
    public GameObject vida1;
    public GameObject vida2;
    public GameObject vida3;
    public GameObject explosion;
    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collider;
    [SerializeField] AudioSource audioSource;

    public float delay = 60;

    public int lives = 3;

    private bool iamDead = false;

    // Update is called once per frame
    void Start()
    {
       
        StartCoroutine(LoadBoss(delay));
    }
    void Update()
    {
        if(iamDead){
            return;
        }
        explosion.gameObject.SetActive(false);
        shootTime += Time.deltaTime;

        transform.Translate(axis * speed * Time.deltaTime);

        if (transform.position.x > limits.x)
        {
            transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
        }
        else if (transform.position.x < -limits.x)
        {
            transform.position = new Vector3(-limits.x, transform.position.y, transform.position.z);
            
        }

        if (transform.position.y > limits.y)
        {
            transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
            
        }
        else if (transform.position.y < -limits.y)
        {
            transform.position = new Vector3(transform.position.x, -limits.y, transform.position.z);

            
        }
        if (axis.x >0)
            {
                prop1.SetActive(true);
            }
            else
            {
                prop1.SetActive(false);
              
            }


    }





    public void ActualizaDatosInput(Vector2 currentAxis)
    {
        axis = currentAxis;
    }

    public void SetAxis(float x, float y)
    {
        axis = new Vector2(x, y);
    }

    public void SetAxis(Vector2 currentAxis)
    {
        axis = currentAxis;
    }

    public void Shoot()
    {
        if (shootTime > weapon.GetCadencia())
        {
            shootTime = 0f;
            weapon.Shoot();
        }
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Laser" || other.tag == "Enemy")
        {
            StartCoroutine(DestroyShip());
        }
        else if (other.tag == "Vidas")
        {
            StartCoroutine(VidasCaja());
            
        }

         

    }

    

    IEnumerator LoadBoss(float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene("Boss");
    }

    IEnumerator DestroyShip(){

        //Indico que estoy muerto
        iamDead = true;

        //Me quito una vida
        lives--;

        //Desactivo el grafico
        graphics.SetActive(false);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Desactivo el propeller
        prop1.gameObject.SetActive(false);

        //Animacion explosion
        explosion.gameObject.SetActive(true);

        //Audio explosion
        audioSource.Play();
        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Corazones
        if (lives == 2)
        {
            vida1.gameObject.SetActive(false);
        }
        if (lives == 1)
        {
            vida2.gameObject.SetActive(false);
        }
        if (lives == 0)
        {
            vida3.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            SceneManager.LoadScene("GameOverScene");

        }

        //Miro si tengo mas vidas
        if (lives>0){
            prop1.gameObject.SetActive(true);
            StartCoroutine(inMortal());

        }
    }
         
        IEnumerator inMortal(){
             iamDead = false;
             graphics.SetActive(true);

            for(int i=0;i<15;i++){
                 graphics.SetActive(false);
                 yield return new WaitForSeconds(0.1f);
                 graphics.SetActive(true);
                 yield return new WaitForSeconds(0.1f);
            }
            collider.enabled = true;
        }

    IEnumerator VidasCaja()
    {

        if (lives == 2)
        {
            vida1.gameObject.SetActive(true);

            lives = 3;
        }
        else if (lives == 1)
        {
            vida2.gameObject.SetActive(true);

            lives = 2;
        }


        yield return new WaitForSeconds(1.0f);

    }

}

