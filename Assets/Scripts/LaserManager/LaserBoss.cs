﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBoss : MonoBehaviour
{
    public float time;
    private float currentTime;
    public GameObject lasser;
    public AudioSource audioSource;

    private int tipodisparo;

    // Update is called once per frame
    void Update()
    {
        currentTime+= Time.deltaTime;

        tipodisparo = Random.Range(1,3);
        if(time<currentTime){

            if(currentTime == 0){
                tipodisparo = Random.Range(1,3);
            }

            if(tipodisparo == 1){
             Instantiate(lasser,transform.position,Quaternion.Euler(0,0,Random.Range(-99,99)),null);
             Instantiate(lasser,transform.position,Quaternion.Euler(0,0,Random.Range(-99,99)),null);
             Instantiate(lasser,transform.position,Quaternion.Euler(0,0,Random.Range(-99,99)),null);
             audioSource.Play();
             currentTime = 0;   
            }
            
        }
    }
}
