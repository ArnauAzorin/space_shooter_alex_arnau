﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserManager2 : MonoBehaviour
{
    public float time;
    private float currentTime;
    public GameObject lasser;
    public AudioSource audioSource;


    // Update is called once per frame
    void Update()
    {
        currentTime+= Time.deltaTime;
        if(time<currentTime){

         Instantiate(lasser,transform.position,Quaternion.identity,null);
         audioSource.Play();
         currentTime = 0;   
            
        }
    }
}
