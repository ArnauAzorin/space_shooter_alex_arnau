﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
   
   public static int scoreValue;

   Text Scoretext;

   void Start() {
       Scoretext = GetComponent<Text>();
   }

    // Update is called once per frame
    void Update()
    {
        Scoretext.text = "000000" + scoreValue;
    }
}
