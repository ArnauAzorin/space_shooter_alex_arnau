﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PulsaPlay()
    {
        SceneManager.LoadScene("GamePlayScreen");
    }
    public void PulsaExit()
    {
        Application.Quit();
    }
    public void PulsaCredits()
    {
        SceneManager.LoadScene("Credits");
    }
    public void PulsaMenu()
    {
        SceneManager.LoadScene("TitleScreen");
    }
    public void PulsaControls()
    {
        SceneManager.LoadScene("ControlScene");
    }
}
